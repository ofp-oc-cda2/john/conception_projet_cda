# conception_projet_cda



# The Attitude Band project

![](images\logo.jpg "Logo")

#
#






>Creation of a website for a rock band with Symfony6
> API rest + Mobile application using Flutter



## Tech

- [Stimulus JS] - Framework JS
- [Flutter] - Framework for mobile app
- [Symfony 6] - Framework PHP
- [GitLab] - Team working and versionning
- [SCSS] - Some style
- [Docker] - Container for developing an app 

## Development

App Functionalities :

- A map with upcoming shows
- The user can ask to be notified of upcoming concerts within a defined radius and a newsletter
- Integrated music player
- Merchandising and payments
- Interface admin
- Account 
- Display social network news

## MCD 

```mermaid
classDiagram

    Department "*" -- "1" Region
    City "*" -- "1" Department
    User "*" -- "1" City
    Order "*" -- "1" User
    Order "1" -- "*" OrdersDetailed
    OrdersDetailed "*" -- "1" Product
    Product "*" -- "1" Images
    City "*" -- "1" Concert


    class User{
    - id : int
    - firstname : String
    - lastname : String
    - email : String
    - password : String
    - address : String
    - profilpicture : String
    - cityid: int
    - verif_email : Boolean = 0
    - verif_account : Boolean = 0
    - SuperAdmin : Boolean = 0
    }

    class Order{
    - id : int
    - userid : int
    - orderconfirm : boolean
    - sendconfirm : boolean
    - userreception : boolean
    }

 class Concert{
    - id : int
    - title : string
    - description : text
    - cityId : int
    }

  class OrdersDetailed{
    - id : int
    - orderid : int
    - productid : int 
    - quantity : int 
    - price : int
    }


    class Product{
    - id : int
    - name : String
    - price : int
    - description : text
    }

    class Images{
    - id : int
    - productid : int 
    - name : string
    }

    class Region{
    - id : int
    - name : String
    - code : String
    - slug : string
    }

    class Department{
    - id : int
    - name : String
    - regionid: int
    - code : String
    - slug : String
    }
    
    class City{
    - id : int
    - name : String
    - departmentid : int
    - zip_code : ?String
    - slug : String
    - gps_lat : Float
    - gps_lng : Float
    }
```

## License

MIT

**Free Software, Hell Yeah!**

